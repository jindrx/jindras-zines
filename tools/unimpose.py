import copy
import sys
import math
import pypdf


def split_pages(src, dst):
    src_f = open(src, 'r+b')
    dst_f = open(dst, 'w+b')

    input_PDF = pypdf.PdfReader(src_f)
    num_pages = len(input_PDF.pages)

    first_half, second_half = [], []

    for i in range(num_pages):
        p = input_PDF.pages[i]
        q = copy.copy(p)
        q.mediabox = copy.copy(p.mediabox)

        x1, x2 = p.mediabox.lower_left
        x3, x4 = p.mediabox.upper_right

        x1, x2 = math.floor(x1), math.floor(x2)
        x3, x4 = math.floor(x3), math.floor(x4)
        x5, x6 = math.floor(x3/2), math.floor(x4/2)

        if x3 > x4:
            # horizontal
            p.mediabox.upper_right = (x5, x4)
            p.mediabox.lower_left = (x1, x2)

            q.mediabox.upper_right = (x3, x4)
            q.mediabox.lower_left = (x5, x2)
        else:
            # vertical
            p.mediabox.upper_right = (x3, x4)
            p.mediabox.lower_left = (x1, x6)

            q.mediabox.upper_right = (x3, x6)
            q.mediabox.lower_left = (x1, x2)


        if i in range(1,num_pages+1,2):
            first_half += [p]
            second_half += [q]
        else:
            first_half += [q]
            second_half += [p]

    output = pypdf.PdfWriter()
    for page in first_half + second_half[::-1]:
        output.add_page(page)

    output.write(dst_f)
    src_f.close()
    dst_f.close()

if len(sys.argv) < 3:
    print("\nusage:\n$ python reverse_impose.py input.pdf output.pdf")
    sys.exit()

input_file = sys.argv[1]
output_file = sys.argv[2]

split_pages(input_file,output_file)

\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[czech]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{pdfpages}
\usepackage[top=2cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{parskip} %Extra space between paragraphs
\setlength{\parindent}{0cm}
\usepackage{tgschola}

\newcommand{\velky}{\fontsize{14.4}{15}\selectfont}
\newcommand{\vetsi}{\fontsize{15}{16}\selectfont}


\newcommand{\pr}{\textbf{Pozn. překl.:} }


\usepackage{xparse}


\let\oldquote\quote
\let\endoldquote\endquote

\RenewDocumentEnvironment{quote}{om}
  {\oldquote\itshape }
  {\par\nobreak\smallskip
   \hfill\textit{#2}\IfValueT{#1}{~---~#1}\endoldquote
   \addvspace{\bigskipamount}}


\usepackage{eso-pic}
\newcommand{\bg}[1]{%
\AddToShipoutPictureBG*{\includegraphics[width=\paperwidth,height=\paperheight]{#1}};
}
\newcommand{\coverback}[1]{%
\AddToShipoutPictureBG*{\includegraphics[width=\paperwidth,height=\paperheight,trim={0 0 21cm 0}]{#1}}%
%14.85 cm
}
\newcommand{\coverfront}[1]{%
\AddToShipoutPictureBG*{\includegraphics[width=\paperwidth,height=\paperheight,trim={21cm 0 0 0}]{#1}}%
}


\usepackage{hyperref}
\urlstyle{same}
%Allows for links with https that is not printed
\include{../../includes/urls.tex}
\usepackage{xurl}

\begin{document}
%\pretolerance=10000
%\tolerance=2000
\pretolerance=5000
\tolerance=9000
\emergencystretch=10pt

\vetsi


\thispagestyle{empty}
\coverfront{./images/cover/cover.png}

\phantom{.}

\newpage
\thispagestyle{empty}

\phantom{.}


\newpage
\thispagestyle{empty}


\vspace*{2cm}
{
\normalsize
Taken from: \url[https://]{theanarchistlibrary.org/library/strangers-in-a-tangled-wilderness-life-without-law}

Design: Jindra,
Version: 1.0.0\\
\url[https://]{gitlab.com/spinkdistro/Zines}\\
\url[https://]{spinkdistro.art}
}

{
\hspace*{\fill}
\includegraphics[width=.3\textwidth]{../../includes/spinkdistro.png}
\hspace*{\fill}
}

\newpage

\setcounter{page}{1}


\begin{quote}{Emma Goldman, 1931}
I want freedom, the right to self-expression, everybody’s right to beautiful, radiant things.
\end{quote}

An anarchist is someone who rejects the domination of one person or class of
people over another. Anarch\emph{ism} is a very broad umbrella term for a group of
political philosophies that are based on the idea that we can live as
anarchists. We anarchists want a world without nations, governments,
capitalism, racism, sexism, homophobia… without any of the numerous,
intersecting systems of domination the world bears the weight of today.

There is no single perfect expression of anarchism, because anarchism is a
network of ideas instead of a single dogmatic philosophy. And we quite prefer
it that way.

\section{The World Today}

\begin{quote}{Octave Mirbeau, 1899}
You’re obliged to pretend respect for people and institutions you think absurd.
You live attached in a cowardly fashion to moral and social conventions you
despise, condemn, and know lack all foundation. It is that permanent
contradiction between your ideas and desires and all the dead formalities and
vain pretenses of your civilization which makes you sad, troubled and
unbalanced. In that intolerable conflict you lose all joy of life and all
feeling of personality, because at every moment they suppress and restrain and
check the free play of your powers. That’s the poisoned and mortal wound of the
civilized world.
\end{quote}

There are those who say that anarchism wouldn’t work, that we need laws and
cops and capitalism. But we say that it is the systems that are currently in
place which aren’t working.

Industrialization is warming the planet to the degree that it might yet just
kill us all. In the best case scenario, we’ve already created one of the
largest mass extinctions in the history of the earth. Deforestation spreads the
deserts in the wild and systemic racism expands the food deserts in the cities.

Billions go hungry every day across the globe because global capitalism makes
it more profitable for the elite of starving nations to grow crops for export
than to feed their own people. Science has been subverted by the demands of
profit, and research is only funded if it explores what might make some rich
bastards richer.

Even the middle class is beginning to fall into ruin, and in this economy,
there aren’t many left who buy into the myth of prosperity that they sold us
when we were kids.

We’re told that anarchy can’t work because people are “inherently” flawed and
are motivated solely by self-interest. They some-how make the illogical jump
from this idea to the idea that we therefore need leaders and government. But
if we don’t trust people to lead themselves, why do we trust them enough to put
them in charge of everybody?

\section{Responsibility and Freedom}

\begin{quote}{Ursula K. Le Guin, 1974}
An anarchist is one who, choosing, accepts the responsibility of choice.
\end{quote}

One way some anarchists like to think about it is that anarchism is the
marriage of responsibility and freedom. In a state society, under the rule of
government, we are held responsible to a set of laws to which we did not
consent. We are expected to be responsible without being trusted with freedom.
There are laws about everything: whom we can love, what imaginary lines we can
cross, what we can put into our own bodies. We are not trusted to act on our
own authority, and at every turn we are being man-aged, observed, policed, and,
if we step out of line, imprisoned.

The reverse—freedom without responsibility—is not much better, and it forms the
mainstream myth of anarchy. Government thrives off this misconception, the idea
that it’s only the existence of cops and prisons that keeps us from murdering
one another wholesale. But in reality, the people in this world who act with
total freedom and no responsibility are those so privileged in our society so
as to be above reproach, such as the police and the ultra-rich. Most of the
rest of us understand that in order to be free, we must hold ourselves
accountable to those we care about and those our actions might impede upon: our
communities and families and friends. 


\section{Anti-Capitalism}

\begin{quote}{Jean-Jacques Rousseau, 1754}
The first man who, having fenced in a piece of land, said “This is mine,” and
found people naïve enough to believe him, that man was the true founder of
civil society. From how many crimes, wars, and murders, from how many horrors
and misfortunes might not any one have saved mankind, by pulling up the stakes,
or filling up the ditch, and crying to his fellows: Beware of listening to this
imposter; you are undone if you once forget that the fruits of the earth belong
to us all, and the earth itself to nobody.
\end{quote}

There’s this idea, which has proven demonstrably false on a global level, that
it’s “good” or “healthy” or “more natural” for most everyone in a society to
act solely for personal gain. In economic terms, this is the central myth of
capitalism: that everyone should try to get one over on everyone else all the
time, and that if everyone does that, most people win. The people who want you
to believe that myth are the people who do win: the people who already control
everything.

Capitalism does not, as is popularly misunderstood, mean an economic system in
which people work for money that they can exchange for goods or services.
Capitalism is, instead, an economic system in which people can leverage their
access to capital to extort money from other people. That is to say, capitalism
is the system by which people who own things don’t have to work and everyone
else does. The owning class makes money just by already having money. They make
money off investments, off renting property, off the value produced by their
employees. They live in luxury because they are in the process of dominating
everyone who makes money through work.

Capitalism is a system by which one class of people dominates another, and we
oppose it. Instead, we suggest all kinds of different ways of organizing our
economies. Some anarchists argue for \textbf{communism}, in which the means of
production are held in common by communes. Others favor \textbf{mutualism}, in which
means of production are owned by individuals or collectives and money is used
but money can only be made through work, not through capital. Still others push
for a system of \textbf{gift economics}, an organic system in which people give to one
another freely and without compulsion, sharing when and what they would like
with whom they’d like. There are many more ideas than this besides, and most
anarchists believe that any given group of anarchists ought to be free to
choose the system that they prefer—as long as these ideas steer clear from
demonstrably oppressive systems like capitalism.

\section{Anti-State}

\begin{quote}{Leo Tolstoy, 1894}
Government is an association of men who do violence to the rest of us.
\end{quote}

For the past several hundred years, the progressive rhetoric in Western
societies has been around what sort of government to have. But the division of
people and geography into “states” under which they are ruled is itself
preposterous and harmful. To an anarchist, asking what sort of government to
have is like asking whether it’s better to be eaten by wolves or lions. What is
not asked often enough is whether or not we ought to be “governed” at all.

Anarchists do not eschew organization, however. If anything, we spend too much
of our time concerned with its intricacies. We are opposed to government
because we are opposed to being ruled, not because we are opposed to organizing
amongst our peers for our mutual benefit.

But this is not to say that what we want is democracy. At its worst, as is
practiced in the US and elsewhere, we have a “representative” democracy in
which we appoint our rulers. At its best, we might hope for a “direct”
democracy in which we all get to vote on decisions. But a democracy is a
government still, one that makes up a set of laws that everyone is compelled to
obey—like when six wolves and four sheep get together to plan what they would
like for dinner.

Amongst ourselves, we create organizing structures that allow for the full
autonomy of every individual, wherein no person can be compelled to go along
with the wishes of the group. Because we are not interested, by and large, in
static organizational structures with fixed and official membership, anarchists
are able to organize organically. People come and go from organizations and the
organizations themselves come and go over time based on the needs of the people
who make use of them. When larger structures are deemed useful or necessary,
various groups often form networks, which are horizontal structures for
disseminating ideas and information and for planning complex operations.


\section{A World Without Law}

\begin{quote}{Anatole France, 1894}
How noble the law, in its majestic equality, that both the rich and poor are
equally prohibited from peeing in the streets, sleeping under bridges, and
stealing bread!
\end{quote}

\begin{quote}{Emma Goldman, 1917}
No great idea in its beginning can ever be within the law. How can it be within
the law? The law is stationary. The law is fixed. The law is a chariot wheel
which binds us all regardless of conditions or place or time.
\end{quote}

Some people have an unfortunate tendency to insist that you can’t be against
something unless you know what you’re for. We reject that idea. We don’t feel
the burden of proof is upon the oppressed to identify what they would like to
replace their oppressor with.

\vspace*{-.34\baselineskip}
If I’m being hit with a baseball bat, I don’t feel the need to articulate what
I would prefer to be hit with instead. Or, more to the point, police hit us
with batons and the media insists that if we wish to stop being hit with batons
we need to articulate exactly how it is we’d like to see crime and punishment
handled within an anarchist society. But while identifying and destroying
systems of domination is the task immediately before us, we do spend some of
our time imagining what a world without law would be like. And occasionally, we
have the chance to enact such a world for days or weeks or years in groups both
big and small and we’ve met with a fair amount of success. A world without law
is not a world without guidelines. We are opposed to law because law is a way
of understanding human conduct that was designed—and has been implemented—for
social control rather than for the furtherance of justice. Laws are designed to
be obscure yet rigid, creating a series of traps for those who are already
disenfranchised by society.

\vspace*{-.34\baselineskip}
A law is not actually a particularly useful tool for judging human behavior. As
the folk wisdom suggests, good people don’t need laws and bad people don’t
follow them. Laws are black and white, forcing people to obey the “letter” of
the law while gleefully ignoring the “spirit.” And what’s more, because they
are enforced through violence at the slightest provocation, they polarize
society into those too afraid to step out of line but without knowing why they
obey and those who disobey simply for the sake of disobeying. Either way, they
hinder people’s ability to develop their own personal sets of ethics. They
don’t help people learn to respect people for the sake of respecting people.

\vspace*{-.34\baselineskip}
People who are encouraged to act socially tend to act socially, and people who
are treated with empathy will, by and large, respond in kind. There will always
be exceptions, of course, but for dealing with those people, guidelines—which
remain mutable to circumstance—are a significantly more useful tool than law
will ever be. Further, many anarchists work towards what is referred to as
“transformative justice.” This is the concept that, while it is impossible to
repair the harm done by the perpetrator of an unjust act, one can work to help
the perpetrator take personal responsibility for what they have done so as to
prevent them from returning to such behavior in the future. An anarchist
society, like any other, will still defend itself from those who cannot or will
not take responsibility for their actions, but this self-defense is done in the
name of protection rather than “punishment” or “revenge.” It’s worth
acknowledging here that like many of our ideas and methods, transformative
justice is practiced—and was developed—not just by anarchists but by a wide
range other marginalized groups.

And of course, we don’t live in an anarchist society, free from the influence
of the culture of domination that surrounds us, and any thoughts we have about
a world without law are reasonably hypothetical. Once more, we reserve the
right to condemn atrocities, like the culture of prison and police, without
feeling an obligation to field and implement fully-developed alternatives. 


\section{Mutual Aid \& Solidarity}

\begin{quote}{Mikhail Bakunin, 1871}
I am truly free only when all human beings, men and women, are equally free.
The freedom of other men, far from negating or limiting my freedom, is, on the
contrary, its necessary premise and confirmation.
\end{quote}

\textbf{Mutual aid} is a fancy way of saying “helping each oth-er out,” and
it’s one of the core anarchist beliefs. We believe that people can interact in
meaningful ways by sharing resources freely, without coercion. We share because
it helps ourselves and everyone around us live more meaningful lives. We put
more stock in cooperation than competition.

\textbf{Solidarity} is a fancy word for “having one another’s backs.” Solidarity is the
most powerful force that the oppressed can bring to bear upon their oppressors.
Every time they come after one of us, we act as though they are coming after
all of us. Solidarity can look like a thousand different things. It can be when
someone tackles a cop to free another protester, it can demonstrations or
actions in the names of those whose voices have been silenced by the state.
Solidarity can be offering childcare for new parents, it can be medical aid.
Solidarity is when we show the world that none of us is alone, when we choose
to intertwine our struggles.

Solidarity is, in some ways, the opposite of charity. Charity is a way of
providing aid that reinforces the hierarchical relationship between groups.
Rich people donating money to charity makes poor people even more dependent
upon the rich. Poor people, however, organizing to share resources as equals,
are acting out of solidarity.

\section{Consent \& Consensus}

\begin{quote}{Pierre-Joseph Proudhon, 1849}
Whoever lays his hand on me to govern me is a usurper and tyrant, and I declare
him my enemy.
\end{quote}

Since we anarchists are committed to only doing things with people that those
people want to do, we utilize a number of methods to determine what those
things are.

On an individual level, we’re interested in practices based on consent. It’s
rather amazing how little mainstream society teaches us to value one another’s
consent.

Consent is a way of finding out what other people are interested in doing with
you. Mostly, this just means asking people before you do things with them. “Do
you want to come to this demonstration?” “Can I kiss you?” “Would you like me
to touch your back?” “Can I help you with that?” Some people consciously
develop non-verbal ways of communicating consent, but the important thing is to
not act without knowing if the other person is informed of the ramifications of
an action, is in a headspace to make decisions, and is enthusiastic.

One tool we use for finding consent in larger groups is consensus.

Most anarchist decision-making is built around this method. Consensus is a way
of determining what everyone in a group is comfortable with doing. “Do we want
to blockade this building?” “Do we want to sign our group’s name on this public
letter?” “Do we want to publish this book?” A group that respects the autonomy
of every individual within it will generally act via consensus in some form or
another. Some people mistake consensus to be basically the same as voting but
where everyone agrees instead of a majority. This thinking however, is still
built around voting, which is a form of competitive decision-making that is not
designed to respect people’s autonomy. Consensus, instead of being a way to
convince everyone to agree to the same plan, is a way of exploring what the
logical limits of any given group are. If all members of a group cannot agree
on a specific action, then it clearly needs to take place outside of that
group, if at all. Unlike consent on an individual level, however, it is not
always the case that a group seeking consensus needs everyone to be
enthusiastic about the given action, and “standing aside” on a decision is
common and respectable behavior.

Not all collectives and groups are very formal in their consensus
decision-making, and many groups tend to work more on an “autonomy” model in
which everyone is trusted to act on behalf of the group and then be responsible
to everyone else for the actions and decisions they made on behalf of the
group.

\section{Direct Action}

\begin{quote}{Lucy Parsons, 1890s}
Anarchists know that a long period of education must precede any great
fundamental change in society, hence they do not believe in vote begging, nor
political campaigns, but rather in the development of self-thinking
individuals.
\end{quote}

Anarchists do not want to reform the existing political system, we want its
abolition. Instead of political advocacy, by which we might appeal to others to
change our conditions, we generally practice direct action.

Direct action is a means by which we take control over our own lives, by which
we regain the autonomy and agency that is systematically stripped away from us
by governmental systems, by which we become self-thinking individuals.

Rather than plead and beg for the government or corporations to start
protecting forests, we put our bodies between the trees and the chainsaws—or
sneak in at night and burn their logging trucks.

No system based on industrialization and capitalism is ever going to prioritize
natural ecosystems over profit, so we won’t waste our time asking nicely.
Rather than ask the capitalists to repeal their trade policies that are gutting
developing nations, we will show up en masse to their summits and block trade
delegates from ever having the chance to scheme. Rather than campaign for the
right to marry, we’ll live our queer lives however we feel with whomever we
choose, and we’ll defend ourselves from bigots instead of asking the state to
intervene.


\section{Prefiguration}

\begin{quote}{Mikhail Bakunin}
If you took the most ardent revolutionary, vested him in absolute power, within
a year he would be worse than the Tsar himself.
\end{quote}

We participate in direct action because we find the means and the ends to be
inseparable. It’s quite likely that none of us will live in an anarchist
society, but that doesn’t mean we can’t act like anarchists now. To be an
anarchist is at least as much about the ways in which you engage with the world
and how you treat people as it is about what fantastic utopia you hope to one
day live in.

Sometimes we call this intertwining of the means and the ends “prefiguration.”
Anarchists aim to act in ways that maximize other people’s autonomy. Most
Marxists, state communists, and other “revolutionary” ideologies suggest a
vanguard with which to seize power. We’ve no interest in seizing power for
anyone but ourselves, and we oppose anyone who thinks they ought to rule us,
“revolutionary” or not.

What’s more, prefiguration means that we don’t put up with oppressive attitudes
in our circles, because we seek a world without oppressive behavior.

It doesn’t mean, however, that we have to be non-violent. While we do believe a
responsible anarchist world would be more peaceful than the world we inhabit
today, most anarchists accept that domination may occasionally need to be met
with violent force in order to stop it. Our problem isn’t with violence itself,
but the systems of domination that make use of it. 

\section{Tactics}

\begin{quote}{Benjamin R. Tucker, 1895}
An anarchist is anyone who denies the necessity and legitimacy of government;
the question of his methods of attacking it is foreign to the definition.
\end{quote}

The same as there is no unified idea of anarchist economics, there is no
universally accepted framework for anarchist tactics. We know we believe in
direct action, but what kinds? Almost every individual anarchist or anarchist
group might respond to this question differently.

The most famous anarchist tactic so far in the twenty-first century is the
black bloc. The black bloc is a tactic by which we obscure our identities by
wearing identical black clothing and then engage in various direct actions,
usually in public. The most iconic action is probably that of breaking out the
windows of banks, court houses, chain stores, and other institutions and
symbols of domination. The second most-well-known action black blocs engage in
is that of defending demonstrations from police attack, often by using shields,
reinforced banners, and the occasional weapon like flagpoles or thrown rocks.
The black bloc tactic remains popular today because it is effective at
empowering those who participate in it and, compared to other tactics,
effective at keeping those involved safe from police repression. This does not
mean that every anarchist participates in—or even supports—black bloc tactics,
nor does it mean that people who participate in black blocs don’t engage in
other tactics as well.

%TODO bad formatting
There are many, many more tactics that anarchists are actively engaged with all
over the world besides wearing black and taking the streets. (We also, for
example, sometimes wear color when we take the streets.) We organize
demonstrations. We organize free dinners for ourselves and anyone else who
needs food. We organize workplaces into unions and we start worker-owned
cooperatives. We work towards cities designed to suit the needs of people and
the ecosystem instead of the desires of the wealthy. We throw pies at
politicians to show the world that they are not untouchable. We run magazines
and blogs and write as journalists. We hack security databases and leak
information to the public about the ways in which the public is being spied
upon. We tell stories that heroize resistance to oppression. We help people
cross borders. We fight fascists in the streets. We’ve been known to burn down
a building or two. And it’s been awhile, but we used to kill kings.

We openly advocate what’s called a “diversity of tactics,” meaning we’ve got as
much respect for those practicing non-violent civil disobedience as we do for
arsonists— that is to say, only as much respect as the individual actions
themselves deserve on their own merit at the time, place, and social context in
which they were used.


\section{Strategy}

\begin{quote}{Aragorn!, 2005}
An anarchist strategy is not a strategy about how to make a capitalist or
statist society less authoritarian or spectacular. It assumes that we cannot
have an anarchist society while the state or capitalism continues to reign.
\end{quote}

A lot of broader strategies have been suggested for how we might go about
creating an anarchist society—or even just strategies of how we might best live
as anarchists here and now. Each has their proponents and detractors, but few
people believe that there is one single correct path to take towards freedom,
and all of these strategies have in the past and will continue to overlap.

The most famous strategy is that of revolution, in which a single, reasonably
organized mass uprising allows for the oppressed classes to seize the means of
production and take their lives into their own hands. Many anarchists remain
skeptical of how we might go about organizing such a thing in a way that
doesn’t simply leave a different class of people, an anarchist government of
sorts, in charge.

Furthermore, exemplified in the recent uprisings in the Middle East, revolution
does not have the best track record in terms of increasing liberty to those in
the revolutionary country. Quite often, state communists or other authoritarian
groups have essentially seized control of the revolution at the last minute,
stepping into the vacuum of power. This, many anarchists would argue, doesn’t
mean that an anti-authoritarian revolution is impossible, only that it faces
numerous challenges.

A second strategy is that of advocating and participating in \textbf{insurrections}.
Insurrections are moments of freedom and revolt, often occurring in times of
crisis. These insurrections would, ideally, allow for areas to be liberated
from state control and, if they came in increasing strength and frequency,
allow for a generalized revolt that could break state power. It has been argued
that insurrections do not provide lasting change and can often simply serve as
an excuse for government repression, but insurrections have also played
important roles in numerous anarchist struggles.

A third strategy that anarchists have historically tried is \textbf{syndicalism}.

This method is a “workerist” method that suggests destroying the capitalist
state economy by means of workers taking direct control of their factories.

While largely popular and indeed, often successful, in the past, the nature of
modern labor and the shift in developed countries away from manufac-turing
makes syndicalism less popular than it has historically been.

Another strategy is referred to sometimes as the \textbf{dual power} strategy.

This is a strategy of building up “counter-infrastructure” along anarchist
lines to fulfill people’s needs and desires while simultaneously attacking the
mainstream institutions that are destroying the world.

This list is clearly not all-inclusive. Some anarchists find themselves
primarily concerned with strategies based around decolonization, education, or
intervening in crisis. Others are likely hard at work scheming strategies that
have never been tried, ideas that we can’t wait to test. 

\section{Engagement With The System}

\begin{quote}{Ursula K Le Guin, 1974}
The individual cannot bargain with the State. The State recognizes no coinage
but power: and it issues the coins itself.
\end{quote}

Obviously, despite our best efforts, we live within a capitalist, statist
world. Anarchism is aspirational and optimistic—it is not, however, delusional.
Just because we do not approve of the state’s existence doesn’t mean we don’t
understand that the state exists and has material power. We don’t “believe” in
prison, but that doesn’t keep the state from locking us inside it. Every action
we take, as individuals and as groups, needs to accept the reality of the
situation. Perhaps if we were perfect anarchists, we would destroy our
state-issued IDs and not pull over the next time a cop puts on their lights
behind us, but we must all make strategic concessions. Similarly, we want a
world without wage labor, but this does not make us hypocrites when we work for
the money we need so that we can eat.

\section{History}

\begin{quote}{Curious George Brigade, 2004}
The anarchists of revolutionary Spain would probably rather we fight our own
struggles today than spend so much time discussing theirs! The Spanish
anarchists were just regular folks, and they did exactly what we’ll do when we
get the opportunity.
\end{quote}

Anarchists are more concerned about the present than the future, because how we
live here and now is more important than some illusory utopia. And we’re more
concerned with the future than the past, because we have control over the
future and we will live in it. But we do have a long and rich history, from
which we can draw inspiration, pride, and numerous lessons.

While anarchist-influenced philosophy may be found throughout and before
recorded history, from certain Taoists, Stoics, Hindus, tribal groups, and
others, it was Pierre-Joseph Proudhon who coined the term in 1840 and was the
first to self-identify as an anarchist.

Anarchists have played an enormous role in revolutions, labor struggles,
uprisings, and culture ever since. In the 1880s, anarchists fighting against
wage labor in the United States got caught up in the fight for the eight-hour
work day. After a series of labor rights culminating in a fight in Haymarket
Square in Chicago, eight anarchists were put on trial explicitly for being
anarchists. Four were hanged and one killed himself in jail as a result. Their
martyrdom changed labor history in the US, the eight-hour workday fight was
won, and anarchism continued to be a strong voice in the labor movement.

At the turn of the century, we killed kings and other heads of state, forever
earning a reputation as bomb-throwers and assassins which some of us wear with
pride and others would prefer to forget.

We fought for revolution in Russia for decades, only to be betrayed when the
Bolsheviks turned around and murdered us in 1917. For three years, from
1918–1921, seven million Ukrainians lived as anarchists until the Bolshevik
army betrayed an alliance and conquered us while we were busy fighting armies
hired by the capitalists.

We had another three-year go of it from 1936–1939, when syndicalist labor
unions took control of Catalonia, a region in Spain, during the Spanish
Revolution. Once again, while anarchists were busy fighting a right-wing
invasion, the Bolshevik-controlled communist party opened fire on us and the
country fell to fascists.

Anarchists were heavily involved in Korean independence from Japanese colonial
rule and labor struggle in South America. We were involved in the Mexican
Revolution. We organized hobos with guns in the US and we robbed banks in
France. And we’ve been involved in numerous art, literary, and music
movements—from André Breton’s involvement in surrealism to Crass’s influence on
punk.

But we cannot be weighed down by the past. We have our own history to make.


\section{Present}

\begin{quote}{Alfredo M. Bonanno, 1998}
Anarchism is not a concept that can be locked up in a word like a gravestone.
It is not a political theory. It is a way of conceiving life, and life, young
or old as we may be, old people or children, is not something definitive: it is
a stake we must play day after day.
\end{quote}

In the past fifteen years, anarchism has been, as a movement, on the up-swing.
First with the anti-globalization demonstrations at the turn of the millennium,
now with the rise of anti-austerity riots and movements across the world,
people are beginning to reject authoritarianism. Which makes sense—capitalism
is quickly destroying everything, and we won’t soon forget what a nightmare the
authoritarians made of revolution, whether the right-wing fascists or the
left-wing Stalinists.

\section{So Let’s Say You Want to Join}

\begin{quote}{Anonymous French Graffiti, 1968}
In a society that has destroyed all adventure, the only adventure left is to destroy that society.
\end{quote}

Anarchism isn’t a membership club. Even as a political ideology, we’re more of
an anti-ideology than we are one with a strict set of rules. So there are no
membership forms to mail in and there are no fees. There are anarchist groups,
all over the world, working on any number of problems that might interest you,
from ecology to social justice, and many of those groups will let you join, or
at least participate in their actions.

But you can also just, well, do it. Find yourself a like-minded group of people
and get to it. Organize all the gardeners in your neighborhood to share produce
for free or organize against a multinational like Walmart moving into town.
Squat a building and steal electricity to throw shows and raise money for
anarchist prisoners. Attack symbols of power. Spread information. Act in the
ways you feel compelled to act.

But the most important things about being an anarchist are: treating other
people with respect, as masters of their own lives; and taking control of your
life, seizing freedom, but remaining responsible to yourself and those you care
about.

As a word of warning, there are predators in the anarchist movement.

Agents of the state infiltrate our movement and do their utmost to destroy it.
They prey upon new people in particular, setting them up to break the law and
then sending them to prison for years or decades. Don’t commit felonious crimes
with anyone you haven’t known for years. Never let anyone convince you that if
you “really cared” about anarchism or some other cause that you’d take some
dangerous action. Read up about what happened to Eric McDavid, David McKay, and
the Cleveland 4.

And even if you’re acting alone or with your closest childhood friends, think
carefully and maturely about the ramifications of any illegal action you might
take. While we cannot let ourselves be paralyzed by fear, we need to remember
that certain types of actions will be treated very, very seriously by the
authorities and far more good can be done from outside of prison than from
within.

But that aside, welcome. We need you. The world needs you. Together we can get
some things done.

\newpage
\section{Further Reading}

\begin{quote}{Oscar Wilde, 1891}
People sometimes inquire what form of government is most suitable for an artist
to live under. To this question there is only one answer. The form of
%TODO bad formatting
government that is most suitable to the artist is no government at all.
\end{quote}

Some cool historical anarchists to look up for fun include: Emma Goldman,
Peter Kropotkin, Mikhail Bakunin, Errico Malatesta, Lucy Parsons, Nicola Sacco,
Bartolomeo Vanzetti, Ricardo Flores Magón, Jules Bonnot, Maria Nikiforova,
Nestor Makhno, Noe Itō, Severino Di Giovanni, Renzo Novatore, Voltairine
DeCleyre, Louis Michel, and Francesc Ferrer.

\subsection{Suggested fiction:}

\begin{itemize}
\item \emph{The Dispossessed}, by Ursula K Le Guin
\item \emph{Bolo’bolo}, by PM.
\item \emph{The Fifth Sacred Thing}, by Starhawk
\item \emph{The Mars Trilogy}, by Kim Stanley Robinson
\item \emph{Woman on the Edge of Time}, by Marge Piercy
\item \emph{Just Passing Through}, by Paco Ignacio Taibo II
\item \emph{V for Vendetta}, by Alan Moore
\end{itemize}

\subsection{Suggested films:}

\begin{itemize}
\item \emph{If a Tree Falls}
\item \emph{Breaking the Spell}
\item \emph{Libertarias}
\item \emph{Land \& Freedom}
\item \emph{Was tun, wenn’s brennt? (What to Do in Case of Fire)}
\end{itemize}

\subsection{Suggested anarchist publishers:}

\begin{itemize}
\item AK Press
\item CrimethInc.
\item Eberhardt Press
\item LBC Books
\item Elephant Editions
\item Strangers In a Tangled Wilderness (that’s us!)
\item Combustion Books
\end{itemize}

\begin{quote}{Buenaventura Durruti, 1936}
We are going to inherit the earth . There is not the slightest doubt about
that. The bourgeoisie may blast and burn its own world before it finally leaves
the stage of history. We are not afraid of ruins. We who ploughed the prairies
and built the cities can build again, only better next time. We carry a new
world, here in our hearts. That world is growing this minute.
\end{quote}

\newpage
\thispagestyle{empty}
\coverback{./images/cover/cover.png}

\phantom{.}

\end{document}

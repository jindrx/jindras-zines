# Zines

A collection of translated or orignal zines. So far only in Czech (English summary of zines TBA)!

Merge requesty s opravenými překlepy atp. jsou vítány!

Aktuální přehled zínů a další informace naleznete na [spinkdistro.art](https://spinkdistro.art/).

Tento repozitář používá [git-lfs](https://docs.gitlab.com/ee/topics/git/lfs/) (Git large file storage) pro
uchovávání pdf souborů atp. Takže po stažení (git clone) je třeba použít git lfs pull.

## Tisk

K tisku doporučujeme použít formát brožurky (Booklet), který zde ke každému
souboru dodáváme. Pak stačí tisknout **oboustranně, obracení na kratší straně**.

Jak vygenerovat brožurku?
1. Přeložit .tex soubor do .pdf souboru pomocí programu [pdflatex](https://jidu.cz/Navody/LaTeX/index.php).
2. Např. pomocí programu [pdfarranger](https://github.com/pdfarranger/pdfarranger) lze pak vytvořit
brožurka (otevřít pdf -> ctrl-A -> v menu pravým klikem jde vybrat možnost na generaci brožurky;
POZOR, brožurka bude ve formátu A3, což je třeba napravit bashovou funkcí `pdfcompress` níže).
3. Pokud chcete převést zín ve formátu brožurky (pro oboustranný tisk s
   převrácením na delší straně) do jednostránkového formátu (např. pro čtení na
   mobilech), lze využít skript tools/unimpose.py. Stačí zavolat
   ```
   python unimpose.py input.pdf output.pdf
   ```
   a dostanete výsledek (v souboru output.pdf). To nebude fungovat na brožurkový formát
   v tomto repozitáře (protože je udělaný pro převrácení po kratší straně)
   ale může se hodit pro zíny získané jinde.


## Verze

Každý zín má vevnitř napsanou svou verzi ve formátu
A.B.C. Číslo A je hlavní verze, která se zvětší o 1 jen při
velkých úpravách (grafický redesign atp.). Číslo B se zvedne
při středních úpravách (doplnění nějakého faktu, změna rozvržení...)
a číslo C se zvedne při opravách nebo quality-of-life změnách.

## Tipy pro design

Např. pro remixování těchto zínů.

### Makefile

Ve složce Zines/ je makefile, která vyčistí všechny pomocné
LaTeX soubory (jako .log), stačí napsat
```
make clean
```

Ve složce každého zínu je pak Makefile popisující, jak zín zkompilovat
a~jak z něj udělat brožurku.

Legacy postup:
Pro brožurku se používá program pdfbook2, který je z~balíčku
`pdfjam-extras` ([GitHub](https://github.com/rrthomas/pdfjam-extras)).
Dříve byl pdfbook2 součástí pdfjam, který je součástí distribuce TeX Live.

### Gimp

- [GIMP export to PDF creates white border line](https://gitlab.gnome.org/GNOME/gimp/-/issues/4585)
- [Guides](https://docs.gimp.org/2.10/en/gimp-concepts-image-guides.html)
- [Align Tool](https://docs.gimp.org/en/gimp-tool-align.html)

### Komprese velikosti obrázků

Obrázky se do paměti počítače neukládají pixel po pixelu (většinou), nýbrž
se ukládají pomocí Fourierovského rozkladu, což je šetrnější na paměť.
Každý další člen Fourierovy řady zajišťuje menší a menší vylepšení kvality
obrázku, proto např. platí, že **při snížení velikosti obrázku o polovinu se jeho kvalita zhorší
o méně než polovinu**.

Zde jsou bashové funkce, které používám pro zmenšení velikosti a téměř nepoznatelného
zhoršení kvality:
```
function pdfcompress () {
gs -o output.pdf  -sDEVICE=pdfwrite  -sPAPERSIZE=a4  -dFIXEDMEDIA  -dPDFFitPage  -dCompatibilityLevel=1.4 $1
}
function jpgcompress () {
jpegoptim -m 80 $1
}
function pngcompress () {
pngquant -Q 80 $1 -o $1
}
```

Přijdete si je do `.bashrc` nebo `.zshrc` a můžete je pak používat. POZOR komprese obrázků jej přepíše.
